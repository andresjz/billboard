    var listApp = angular.module('mainScreens', ['monospaced.qrcode']);    
    var url_base = "http://lsys.mx/cartelera/services/";
    var mensaje= 0;

    listApp.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
    });
    /* variable listApp is  a variable which used to control the array values to show the data to show in view  using the module name 'listApp' with arguments as an array */

    /* Initialize the controller by name 'PhoneListCtrl' holds the information of phone in form of array with keys name, snipper, price , quantity */

    /* $scope argument passed in function is a key arguments should be passed with exactly the same name */

listApp.controller('screens', function ($scope, $interval, $http) {

    $scope.filteredItems =  [];
    $scope.groupedItems  =  [];
    $scope.itemsPerPage  =  3;
    $scope.cards    =  [];
    $scope.currentPage   =  0;

    $scope.estadoMensaje = 0;

    $interval( function(){ $scope.revisarCambios(); }, 60000 );
    
    $scope.revisarCambios = function(){
        $scope.get_cards(); 
    };

    $scope.reset_reg_form = function(){
        document.getElementById("regForm").reset();
        $scope.regForm.$setPristine();
    };

   $scope.reset_edit_form = function(){
        document.getElementById("editForm").reset();
        $scope.editForm.$setPristine();     
   };

    /** function to get detail of notification added in mysql referencing php **/
    $scope.get_cards = function(){
    $http.get(url_base +"souls/getCards").success(function(data)
    {
        $scope.cards = data;    
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 5; //max no of items to display in a page
        $scope.filteredItems = $scope.cards.length; //Initially for no filter  
        $scope.totalItems = $scope.cards.length;

    })
    .error(function(data, status, headers, config){
            mensaje = 2;
    })
    ;
    }

    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };

   
});