
var quoteBook = [
  "Forgive me for stating the obvious, but the world is made up of all kinds of people. - Haruki Murakami",
  "You never know what worse luck your bad luck has saved you from. - Cormac McCarthy",
  "The road to hell is paved with adverbs. - Stephen King",
  "It is sometimes an appropriate response to reality to go insane. - Philip K. Dick",
  "I come from Des Moines. Somebody had to. - Bill Bryson",
  "It is a rather risky matter to discuss a happiness that has no need of words. - Yukio Mishima",
  "Integrity's a neutral value. Hyenas have integrity, too. They're pure hyena. - Jonathan Franzen",
  "There's a fine line between a stream of consciousness and a babbling brook to nowhere. - Dan Harmon",
  "Danger's over, Banana Breakfast is saved. - Thomas Pynchon",
  "You can lead a horse to water, but you can't make him sing opera. - Richard Ford",
  "Weird doors open. People fall into things. - David Sedaris",
  "I confess, I do not believe in time. - Vladimir Nabokov",
  "I love sleep. My life has the tendency to fall apart when I'm awake, you know? - Ernest Hemingway",

];

randomQuote = function() {
  return Math.floor(Math.random() * quoteBook.length);
}

$(function() {
  $('#quote').text(quoteBook[randomQuote()]);

  $('#quoteButton').click(function() {
    $('#quote').text(quoteBook[randomQuote()]);

  });
});
