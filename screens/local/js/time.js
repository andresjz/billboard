// promise.js for the browsers not yet supporting promises in Javascript
window.setImmediate||function(){"use strict";function e(e){var t=e[0];switch(e.length){case 1:return t();case 2:return t(e[1]);case 3:return t(e[1],e[2])}return t.apply(window,i.call(e,1))}function t(t){var n,a=t.data;"string"==typeof a&&0==a.indexOf(s)&&(n=r[a],n&&(delete r[a],e(n)))}var n=0,r={},a=!0,i=Array.prototype.slice,s="setImmediatePolyfillMessage";window.setImmediate=function(){for(var e=n++,i=s+e,o=arguments.length,d=new Array(o);o--;)d[o]=arguments[o];return r[i]=d,a&&(a=!1,window.addEventListener("message",t)),window.postMessage(i,"*"),e},window.clearImmediate=function(e){delete r[s+e]}}();

!function(t){function e(){}function n(t,e){return function(){t.apply(e,arguments)}}function o(t){if("object"!=typeof this)throw new TypeError("Promises must be constructed via new");if("function"!=typeof t)throw new TypeError("not a function");this._state=0,this._handled=!1,this._value=void 0,this._deferreds=[],s(t,this)}function r(t,e){for(;3===t._state;)t=t._value;return 0===t._state?void t._deferreds.push(e):(t._handled=!0,void a(function(){var n=1===t._state?e.onFulfilled:e.onRejected;if(null===n)return void(1===t._state?i:f)(e.promise,t._value);var o;try{o=n(t._value)}catch(r){return void f(e.promise,r)}i(e.promise,o)}))}function i(t,e){try{if(e===t)throw new TypeError("A promise cannot be resolved with itself.");if(e&&("object"==typeof e||"function"==typeof e)){var r=e.then;if(e instanceof o)return t._state=3,t._value=e,void u(t);if("function"==typeof r)return void s(n(r,e),t)}t._state=1,t._value=e,u(t)}catch(i){f(t,i)}}function f(t,e){t._state=2,t._value=e,u(t)}function u(t){2===t._state&&0===t._deferreds.length&&a(function(){t._handled||d(t._value)});for(var e=0,n=t._deferreds.length;n>e;e++)r(t,t._deferreds[e]);t._deferreds=null}function c(t,e,n){this.onFulfilled="function"==typeof t?t:null,this.onRejected="function"==typeof e?e:null,this.promise=n}function s(t,e){var n=!1;try{t(function(t){n||(n=!0,i(e,t))},function(t){n||(n=!0,f(e,t))})}catch(o){if(n)return;n=!0,f(e,o)}}var l=setTimeout,a="function"==typeof setImmediate&&setImmediate||function(t){l(t,0)},d=function(t){"undefined"!=typeof console&&console&&console.warn("Possible Unhandled Promise Rejection:",t)};o.prototype["catch"]=function(t){return this.then(null,t)},o.prototype.then=function(t,n){var o=new this.constructor(e);return r(this,new c(t,n,o)),o},o.all=function(t){var e=Array.prototype.slice.call(t);return new o(function(t,n){function o(i,f){try{if(f&&("object"==typeof f||"function"==typeof f)){var u=f.then;if("function"==typeof u)return void u.call(f,function(t){o(i,t)},n)}e[i]=f,0===--r&&t(e)}catch(c){n(c)}}if(0===e.length)return t([]);for(var r=e.length,i=0;i<e.length;i++)o(i,e[i])})},o.resolve=function(t){return t&&"object"==typeof t&&t.constructor===o?t:new o(function(e){e(t)})},o.reject=function(t){return new o(function(e,n){n(t)})},o.race=function(t){return new o(function(e,n){for(var o=0,r=t.length;r>o;o++)t[o].then(e,n)})},o._setImmediateFn=function(t){a=t},o._setUnhandledRejectionFn=function(t){d=t},"undefined"!=typeof module&&module.exports?module.exports=o:t.Promise||(t.Promise=o)}(this);

//icons.json - Temporariy placement here for codepen only

data={"200":{"label":"thunderstorm with light rain","icon":"storm-showers"},"202":{"label":"thunderstorm with heavy rain","icon":"storm-showers"},"210":{"label":"light thunderstorm","icon":"storm-showers"},"211":{"label":"thunderstorm","icon":"thunderstorm"},"212":{"label":"heavy thunderstorm","icon":"thunderstorm"},"221":{"label":"ragged thunderstorm","icon":"thunderstorm"},"230":{"label":"thunderstorm with light drizzle","icon":"storm-showers"},"231":{"label":"thunderstorm with drizzle","icon":"storm-showers"},"232":{"label":"thunderstorm with heavy drizzle","icon":"storm-showers"},"300":{"label":"light intensity drizzle","icon":"sprinkle"},"301":{"label":"drizzle","icon":"sprinkle"},"302":{"label":"heavy intensity drizzle","icon":"sprinkle"},"310":{"label":"light intensity drizzle rain","icon":"sprinkle"},"311":{"label":"drizzle rain","icon":"sprinkle"},"312":{"label":"heavy intensity drizzle rain","icon":"sprinkle"},"313":{"label":"shower rain and drizzle","icon":"sprinkle"},"314":{"label":"heavy shower rain and drizzle","icon":"sprinkle"},"321":{"label":"shower drizzle","icon":"sprinkle"},"500":{"label":"light rain","icon":"rain"},"501":{"label":"moderate rain","icon":"rain"},"502":{"label":"heavy intensity rain","icon":"rain"},"503":{"label":"very heavy rain","icon":"rain"},"504":{"label":"extreme rain","icon":"rain"},"511":{"label":"freezing rain","icon":"rain-mix"},"520":{"label":"light intensity shower rain","icon":"showers"},"521":{"label":"shower rain","icon":"showers"},"522":{"label":"heavy intensity shower rain","icon":"showers"},"531":{"label":"ragged shower rain","icon":"showers"},"600":{"label":"light snow","icon":"snow"},"601":{"label":"snow","icon":"snow"},"602":{"label":"heavy snow","icon":"snow"},"611":{"label":"sleet","icon":"sleet"},"612":{"label":"shower sleet","icon":"sleet"},"615":{"label":"light rain and snow","icon":"rain-mix"},"616":{"label":"rain and snow","icon":"rain-mix"},"620":{"label":"light shower snow","icon":"rain-mix"},"621":{"label":"shower snow","icon":"rain-mix"},"622":{"label":"heavy shower snow","icon":"rain-mix"},"701":{"label":"mist","icon":"sprinkle"},"711":{"label":"smoke","icon":"smoke"},"721":{"label":"haze","icon":"day-haze"},"731":{"label":"sand, dust whirls","icon":"cloudy-gusts"},"741":{"label":"fog","icon":"fog"},"751":{"label":"sand","icon":"cloudy-gusts"},"761":{"label":"dust","icon":"dust"},"762":{"label":"volcanic ash","icon":"smog"},"771":{"label":"squalls","icon":"day-windy"},"781":{"label":"tornado","icon":"tornado"},"800":{"label":"clear sky","icon":"sunny"},"801":{"label":"few clouds","icon":"cloudy"},"802":{"label":"scattered clouds","icon":"cloudy"},"803":{"label":"broken clouds","icon":"cloudy"},"804":{"label":"overcast clouds","icon":"cloudy"},"900":{"label":"tornado","icon":"tornado"},"901":{"label":"tropical storm","icon":"hurricane"},"902":{"label":"hurricane","icon":"hurricane"},"903":{"label":"cold","icon":"snowflake-cold"},"904":{"label":"hot","icon":"hot"},"905":{"label":"windy","icon":"windy"},"906":{"label":"hail","icon":"hail"},"951":{"label":"calm","icon":"sunny"},"952":{"label":"light breeze","icon":"cloudy-gusts"},"953":{"label":"gentle breeze","icon":"cloudy-gusts"},"954":{"label":"moderate breeze","icon":"cloudy-gusts"},"955":{"label":"fresh breeze","icon":"cloudy-gusts"},"956":{"label":"strong breeze","icon":"cloudy-gusts"},"957":{"label":"high wind, near gale","icon":"cloudy-gusts"},"958":{"label":"gale","icon":"cloudy-gusts"},"959":{"label":"severe gale","icon":"cloudy-gusts"},"960":{"label":"storm","icon":"thunderstorm"},"961":{"label":"violent storm","icon":"thunderstorm"},"962":{"label":"hurricane","icon":"cloudy-gusts"}};


// weather.js

var currentWeather, city;

/**
 * @summary Sends HTTP request to get data from url using promise.
 *
 * @param string $url Url for fetching data from.
 *
 * @return object Either data fetched from url is returned or error is returned.
 */
function getData( url ) {
	return new Promise( function ( resolve, reject ) {
	var req = new XMLHttpRequest();
	req.open( 'GET', url, true );
	req.onload = function() {
		if ( req.status == 200 ) {
			resolve( req.response );
		}

	};



	req.send();
	});
}

/**
 * @summary Initiates and completes the process of fetching data and displaying it.
 */
function init() {
	getLocation()
		.then ( function( response ) {
			setLocation( response );
			getWeatherData().then ( function( response ){
				setWeather( response );
				getIcon( response );
				setCurrentTime( response );
				setOtherInfo( response );
				})
				/*.catch( function( error ) {
					document.write ( 'Error: ', error );
				});*/
			})
		.catch( function( error ) {
			document.write ( 'Error: ', error );
		});
}

/**
 * @summary Used to get location info.
 *
 * @return object Promise is returned after fetching data.
 */
function getLocation() {

	locationApiurl = "http://ipinfo.io/json";
	return getData( locationApiurl );
}

/**
 * @summary Sets location of the user by parsing the reposnse object.
 *
 * @param object $locationData location response object returned by request.
 *
 * @global string $city city is set to be used in getting weather data.
 */
function setLocation( locationData ) {
  console.log(locationData);
	loc = JSON.parse( locationData ) ;
	city = loc.city + "," + loc.country;
}

/**
 * @summary Sends request to get weather data.
 *
 * @return object Promise object is returned ofter getting weather data.
 */
function getWeatherData() {
	var weatherApiUrl = "http://api.openweathermap.org/data/2.5/weather?q=";
	var weatherAppId = "&APPID=7040a1f543f50652aee32c6eeedc83e7";
	var units = "&units=metric";
	return getData( weatherApiUrl+ "Córdoba,MX" + weatherAppId + units );
}

/**
 * @summary Sorts out the city name & weather and display it in the application.
 *
 * @param object $weatherObject weather response object returned by request.
 */
function setWeather( weatherObject ) {
	var rawData = JSON.parse( weatherObject );
	var weather = [];
	for ( i = 0; i < rawData.weather.length; i++ ) {
		weather[i] = rawData.weather[i].description;
	}
	currentWeather = {
	"city": rawData.name,
	"country": rawData.sys.country,
	"weather": weather,
	"temperature": Math.ceil( rawData.main.temp )}
	document.getElementById( 'city' ).innerHTML = rawData.name + ", " + rawData.sys.country;
	document.getElementById( 'temperature' ).innerHTML = currentWeather["temperature"];
	/*document.getElementById( 'weather' ).innerHTML = weather.toString();   PARTE EN INGLES ELIMINADA*/
}

/**
 * @summary Sets icon for weather.
 *
 * @param object $resp weather response object returned by request.
 */
function getIcon( resp ) {
	response = JSON.parse( resp );
	var weatherIcons =  data;
	var prefix = 'wi wi-';
	var code = response.weather[0].id;
	var icon = weatherIcons[ code ].icon;
	var responseIcon = response.weather[0].icon;
	var dayOrNight = responseIcon.charAt( responseIcon.length-1 ) === "d" ? "day" : "night";
	// If we are not in the ranges mentioned above, add a day/night prefix.
	if ( !( code > 699 && code < 800 ) && !( code > 809 && code < 1000 ) ) {
		icon === "sunny" ? icon = 'day-' + icon :  icon = dayOrNight + '-' + icon;
		//icon = dayOrNight + '-' + icon;
	}
	// Finally tack on the prefix.
	icon = prefix + icon;
	document.getElementById( 'icon' ).className =  icon;
}

/**

 * @summary Toggles the temperature to Celsius/Farenhiet.
 */
function changeUnit() {
	var currentUnit = document.getElementById( 'unit' ).innerHTML;
	if( currentUnit == "C" ) {
		document.getElementById( 'unit' ).innerHTML = "F";
		document.getElementById( 'temperature' ).innerHTML =  cToF ( currentWeather["temperature"] );
	} else {
		document.getElementById( 'unit' ).innerHTML = "C";
		document.getElementById( 'temperature' ).innerHTML =  currentWeather["temperature"];
	}
}

/**
 * @summary Converts the value from celsius to Farenheit.
 *
 * @param number $celsius temperature to be converted.
 *
 * @return number temperature in Farenheit.
 */
function cToF(celsius) {
	return  Math.ceil( celsius * 9 / 5 + 32 );
}

/**
 * @summary Sets the current date and time.
 *
 * @param obhject $weatherObj weather response object returned by request.
 */
function setCurrentTime( weatherObj ) {
	var weatherData = JSON.parse( weatherObj );
	time = unixToTime( weatherData.dt );
	document.getElementById( 'date' ).innerHTML = time;
}

/**
 * @summary sets value for the additional information to be displayed.
 *
 * @param object $weatherObj weather response object returned by request.
 */
function setOtherInfo( weatherObj ) {
	var weatherData = JSON.parse( weatherObj );
	document.getElementById( 'sunset' ).innerHTML += unixToTime( weatherData.sys.sunset, "time" );
	document.getElementById( 'sunrise' ).innerHTML += unixToTime( weatherData.sys.sunrise, "time" );
	document.getElementById( 'humidity' ).innerHTML += weatherData.main.humidity+"%";
	document.getElementById( 'wind' ).innerHTML += weatherData.wind.speed+"mps";
}

/**
 * @summary Converts time from Unix timestamp to human readable format.
 *
 * @param number $timestamp timestamp to be converted into time and date.
 * @param string $returnType codes to decide the format to be returned.
 *
 * @return string Formatted Date/Time.
 */
function unixToTime( timestamp, returnType ) {
	var a = new Date( timestamp * 1000 );
	var months = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
	var days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var day = days[a.getDay()];
	var hour = a.getHours();
	var mins = a.getMinutes()< 10 ? '0' + a.getMinutes() : a.getMinutes();
	//var sec = a.getSeconds() < 10 ? '0' + a.getSeconds() : a.getSeconds();
	var CurrentDate = day + ", " + date + ' ' + month + ' ' + year;
	var time;
	var dateTime = CurrentDate;
	switch(returnType) {
		case "time":
		return time;
		case "date":
		return CurrentDate;
		default:
		return dateTime;
	}
}

init();




function startTime() {
  var today = new Date();
  var hour = today.getHours();
  var min = today.getMinutes();
  var sec = today.getSeconds();

  min = checkTime(min);
  sec = checkTime(sec);
  document.getElementById('time').innerHTML= hour + ":" + min;
  t=setTimeout(function() {startTime()},500);
}

function checkTime(i) {
  if (i<10) {
    i="0" + i;
  }
  return i;
}
