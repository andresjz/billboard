<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Trimatrix Lab">
    <meta name="description" content="">
    <meta name="keywords" content="">


    <title>Esquela</title>
    <link rel="icon" href="images/site/fav-icon.png">

    <!--APPLE TOUCH ICON-->
    <link rel="apple-touch-icon" href="images/site/apple-touch-icon.png">


    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:500' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>


    <!-- MATERIAL ICON FONT -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- FONT AWESOME -->
    <link href="stylesheets/vendors/font-awesome.min.css" rel="stylesheet">


    <!-- ANIMATION -->
    <link href="stylesheets/vendors/animate.min.css" rel="stylesheet">

    <!-- MAGNIFICENT POPUP -->
    <link href="stylesheets/vendors/magnific-popup.css" rel="stylesheet">

    <!-- SWIPER -->
    <link href="stylesheets/vendors/swiper.min.css" rel="stylesheet">


    <!-- MATERIALIZE -->
    <link href="stylesheets/vendors/materialize.css" rel="stylesheet">
    <!-- BOOTSTRAP -->
    <link href="stylesheets/vendors/bootstrap.min.css" rel="stylesheet">


    <!-- CUSTOM STYLE -->
    <link href="stylesheets/style.css" id="switch_style" rel="stylesheet">
    <!--TOGGLE-->
    <link href="toggle/toggle.css" rel="stylesheet">

</head>
<body id="App" ng-controller="esquelaCtrl">

<div ng-repeat="soul in soulsArray">


<!--==========================================
                    HEADER
===========================================-->
<header id="home">
    <!--HEADER BACKGROUND-->
    <div class="header-background section"></div>

</header>


<!--==========================================
                   V-CARD
===========================================-->
    
<div id="v-card-holder" class="section">
    
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">

                <div id="v-card" class="card">

                    <div id="profile" class="right">
                        <img alt="profile-image" class="img-responsive" src="{{imagen(soul.path_img)}}">
                        <div class="slant"></div>

                    </div>
                    
                    <div class="card-content">

                        <div class="info-headings">
                            <h4 class="text-uppercase left">† {{soul.full_name}}</h4>
                            <h6 class="text-capitalize left">{{verAno(soul.starting_date)}}</h6>
                            <h4 class="text-uppercase left">"El que cree en mi no morirá, vivirá para siempre"</h4>
                        </div>
                        <!-- CONTACT INFO -->
                        <div class="infos">
                            <ul class="profile-list">
                            
                                <li class="clearfix">
                                    <span class="title"><i class="material-icons">language</i></span>
                                    <span class="content">{{soul.type_service}}</span>
                                </li>
                                
                                <li class="clearfix">
                                    <span class="title"><i class="material-icons">place</i></span>
                                    <span class="content" ng-show="soul.id_service != 1">{{ nombreSala(soul.id_service) }}</span>
                                    <span class="content" ng-show="soul.id_service == 1">{{ soul.address }}</span>
                                </li>

                            </ul>
                        </div>

                        
                    </div>
                    <!-- <video id="html-video" class="video" poster="images/poster/poster.jpg" controls>
                        <source src="videos/b.webm" type="video/webm">
                        <source src="videos/a.mp4" type="video/mp4">
                    </video>-->

                </div>
            </div>
        </div>
    </div>
</div>


<!--==========================================
                   ABOUT
===========================================-->
<div id="about" class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- DETAILS -->
                <div id="about-card" class="card">
                    <div class="card-content">
                        <!-- ABOUT PARAGRAPH -->
                        <center><P>
                            Su <b>familia </b> lamenta comunicar el sensible fallecimiento de su ser querido </P>
                            <h4>{{soul.full_name}}. </h4><p>Acaecido el día {{formatoFecha(soul.starting_date)}} en la Ciudad de Córdoba, Veracruz.</p></center>
                    </div>

                    <!-- BUTTONS -->
                    
                </div>
            </div>
        </div>
    </div>
</div>


<!--==========================================
                   EDUCATION
===========================================-->
<section id="education" class="section">
    <div class="container">
        <!-- SECTION TITLE -->
        <div class="section-title">
            <h4 class="text-uppercase text-center"><img src="images/icons/book.png" alt="demo">Celebraciones</h4>
        </div>

        <div id="timeline-education">

            <!-- FIRST TIMELINE -->
            <div class="timeline-block" ng-repeat="c in celebrationsArray | filter: celebration">
                <!-- DOT -->
                <div class="timeline-dot"><h6>†</h6></div>
                <!-- TIMELINE CONTENT -->
                <div class="card timeline-content" >
                    <div class="card-content">
                        <!-- TIMELINE TITLE -->
                        <h6 class="timeline-title">{{c.name}}</h6>
                        <!-- TIMELINE TITLE INFO -->
                        <div class="timeline-info">
                            <h6>
                                <small>{{c.place}}</small>
                            </h6>
                            <h6>
                                <small>{{formatoFecha(c.start)}} a las {{ c.start[11] + c.start[12] + c.start[13] + c.start[14] + c.start[15]  }}</small>
                            </h6>
                        </div>
                        <!-- TIMELINE PARAGRAPH -->
                        
                    </div>
                </div>
                <br><br>
            </div>

            
        </div>
    </div>
</section>




<!--==========================================
             TESTIMONIALS AND CLIENTS
===========================================-->
<section id="testimonials" class="section">
    <div class="container">
        <!-- SECTION TITLE -->
        <div class="section-title">
            <h4 class="text-uppercase text-center"><img src="images/icons/handshake.png" alt="demo">Condolencias</h4>
        </div>
        <div id="testimonials-card" class="row card">
            <div class="col-md-12 col-xs-12">
<div class="card">
                    <div class="card-content">
                        <form id="contact-form" name="c-form">
                            <!-- NAME -->
                            <div class="input-field">
                                <input id="first_name" type="text" class="validate" name="first_name" required>
                                <label for="first_name">Nombre</label>
                            </div>
                            <!--SUBJECT-->
                            <div class="input-field">
                                <input id="sub" type="text" class="validate" name="sub">
                                <label for="sub">Asunto</label>
                            </div>
                            <!--EMAIL-->
                            <div class="input-field">
                                <input id="email" type="email" class="validate" name="email" required>
                                <label for="email">Email</label>
                            </div>
                            <!--MESSAGE-->
                            <div class="input-field">
                                <select id="myOptions">
                                    <option value="1"><span>Te envío mi más sentido pésame. Saluda afectuosamente a los más cercanos, y ya sabes que ahora, más que nunca, puedes contar con mi fiel y sincera amistad.</span></option>
                                    <option value="2">Una gran persona, ejemplo vital para otras muchas; que su recuerdo permanezca y su trayectoria sea recordada. Condolencias a toda la familia.
</option>
                                    <option value="3">En este momento de dolor quiero acompañarte y decirte que eres muy importante para mi y que puedes contar conmigo.</option>
                                </select>
                            </div>
                            <!-- SEND BUTTON -->
                            <div class="contact-send">
                                <button id="submit" name="contactSubmit" type="submit" value="Submit"
                                        class="btn waves-effect">Enviar condolencias
                                </button>
                            </div>
                        </form>
                    </div>
                    <!--FORM LOADER-->
                    <div id="form-loader" class="progress is-hidden">
                        <div class="indeterminate"></div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>
</section>


<!--==========================================
                  CONTACT
===========================================-->
<section id="contact" class="section">
    <div class="container">
        <!-- SECTION TITLE -->
        <div class="section-title">
            <h4 class="text-uppercase text-center"><img src="images/icons/envelope.png" alt="demo">Grupo Vélez Córdoba</h4>
        </div>
        <div class="row">
            <div id="contact-card" class="col-md-5 col-sm-12 col-xs-12">
                <!-- CONTACT FORM -->
                <div class="card">
                    <div class="card-content">
                        
                        <address>
                          <strong>Matriz Córdoba</strong><br>
                          Av. 5 #610 entre calles 6 y 8 Col. Centro<br>
                         Córdoba, Veracruz<br>
                          <abbr title="Phone"></abbr> (271) 7128545
                          <abbr title="Phone"></abbr> (271) 7128586
                        </address>

                        <address>
                          <strong>Sucursal Centro Córdoba</strong><br>
                          Av. 3 # Esq. Calle 11 Col. Centro<br>
                          Córdoba, Veracruz<br>
                          <abbr title="Phone"></abbr> (271) 7120817
                        </address>

                    </div>
                    <!--FORM LOADER-->
                    <div id="form-loader" class="progress is-hidden">
                        <div class="indeterminate"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-7 col-sm-12 col-xs-12">
                <!-- CONTACT MAP -->
                <div id="map-card" class="card">
                    <!-- MAP -->
                    <div id="myMap"></div>
                </div>
            </div>

        </div>
    </div>
</section>

</div>

<!--==========================================
                     SCROLL TO TOP
===========================================-->
<div id="scroll-top">
    <div id="scrollup"><i class="fa fa-angle-up"></i></div>
</div>

<!--==========================================
                      FOOTER
===========================================-->

<footer>
    <div class="container">
        <!--FOOTER DETAILS-->
        <p class="text-center">
            © <a href="www.distritodigital.mx">Distrito Digital.</a> 
                <strong>EsquelasDigitales1.0</strong>
        </p>
    </div>
</footer>




<!--==========================================
                  SCRIPTS
===========================================-->
<script src="javascript/vendors/jquery-2.1.3.min.js"></script>
<script src="javascript/vendors/bootstrap.min.js"></script>
<script src="javascript/vendors/materialize.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI14J_PNWVd-m0gnUBkjmhoQyNyd7nllA"></script>
<script src="javascript/vendors/markerwithlabel.min.js"></script>
<script src="javascript/vendors/retina.min.js"></script>
<script src="javascript/vendors/scrollreveal.min.js"></script>
<script src="javascript/vendors/swiper.jquery.min.js"></script>
<script src="javascript/vendors/jquery.magnific-popup.min.js"></script>
<script src="javascript/custom.js"></script>
<script src="toggle/js.js"></script>

        <script>
            var varURL = {
                id : <?php echo json_encode($_GET["id"]); ?>
            };
        </script>

        <script type="text/javascript" src="../admin/app/angular.min.js"></script>
        <script type="text/javascript" src="../admin/app/app.js"></script>
        <script type="text/javascript" src="../admin/app/controllers.js"></script>


</body>

</html>