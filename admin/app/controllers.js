function nombreEstado(idStatus){

  if(idStatus == 0 )

    return "En proceso";

  else

    return "Finalizado";

}



function colorEstado(idStatus){

  if(idStatus == 0)

    return "warning";

  else

    return "success";

}



function nombreAvailable(id){

  if(id == 0)

    return "Ocupado";

  else

    return "Disponible";

}



function colorAvailable(id){

  if(id == 0)

    return "danger";

  else

    return "success";

}



function errorFechas(hoy, futuro){

  if(futuro <= hoy)

    return 1;

  else

    return 0;

}



var mainURL = "http://lsys.mx/cartelera/services/";


// Incio directivas subir archivo
          /*
          app.directive('fileModel', ['$parse', function ($parse) {
            return {
               restrict: 'A',
               link: function(scope, element, attrs) {
                  var model = $parse(attrs.fileModel);
                  var modelSetter = model.assign;
                  
                  element.bind('change', function(){
                     scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                     });
                  });
               }
            };
         }]);
      
         app.service('fileUpload', ['$http', function ($http) {
            this.uploadFileToUrl = function(file, uploadUrl){
               var fd = new FormData();
               fd.append('file', file);
            
               $http.post(uploadUrl, fd, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
               })
            
               .success(function(){
               })
            
               .error(function(){
               });
            }
         }]);
      
         app.controller('myCtrl', ['$scope', 'fileUpload', function($scope, fileUpload){
            $scope.uploadFile = function(){
               var file = $scope.myFile;
               
               console.log('file is ' );
               console.dir(file);
               
               var uploadUrl = "/fileUpload";
               fileUpload.uploadFileToUrl(file, uploadUrl);
            };
         }]);*/

// Fin directivas subir archivo 


app.controller('serviciosCtrl', function($scope, $http, $sce, REST) {



    $scope.lastID = {"id" : 0};



    $scope.soulsArray = [];

    $scope.roomsArray = [];

    $scope.religionsArray = [];



    $scope.roomsAvailable = [];



    var soulsAPI = new REST(mainURL, "souls");

    var roomsAPI = new REST(mainURL, "rooms");

    var religionsAPI = new REST(mainURL, "religions");



    soulsAPI.getData($scope.soulsArray);

    roomsAPI.getData($scope.roomsArray);

    religionsAPI.getData($scope.religionsArray);



    $scope.labelEstado = function(idStatus){

      return colorEstado(idStatus);

    }



    $scope.estado = function(idStatus){

      return nombreEstado(idStatus);

    }



    $scope.filterRooms = function(){

      $scope.roomsAvailable = $scope.roomsArray.filter(function(s) {

        return s.available == "1";

      })

    }



    $scope.registrarSoul = function(){



      $scope.formSoul.starting_date = getFechaActual() + " " + getHoraActual();

      $scope.formSoul.ending_date = $scope.formSoul.ending_date + " " + $scope.hora;



      if( errorFechas($scope.formSoul.starting_date, $scope.formSoul.ending_date) ){

        alert("Estas ingresando una fecha de finalización errónea (Pasado)\nFecha de registro:" + $scope.formSoul.starting_date);

      }

      else{



        $("#con-close-modal").modal('hide');



        $scope.formSoul.status = 0;

        $scope.formSoul.path_img = "assets/images/flor.jpg";


        //Insertar servicio

        soulsAPI.postData($scope.formSoul, $scope.lastID, $scope.soulsArray);



        //Si es un servicio local

        if($scope.formSoul.id_service != "1"){

          //Obtener la sala

          var roomService = [];



          roomService = $scope.roomsArray.filter(function(s) {

            return s.id == $scope.formSoul.id_service;

          })

          //Poner en ocupado la sala

          roomService[0].available = "0";



          //Editar el status de la sala

          roomsAPI.updateData(roomService[0]);

        }



        $scope.formSoul = {};



      }



    }



    $scope.eliminarSoul = function(id, soul){

      var flag = confirm("¿Estás seguro que quieres borrar el servicio #" + id + "?");

      

      if(flag){

        //Arreglo JSON para acomodar el elemento y que el php pueda reconocerlo como objeto JSON

        soulsAPI.deleteData(id);



        //Si es un servicio local

        if(soul.id_service != "1"){

          //Obtener la sala

          var roomService = [];

          roomService = $scope.roomsArray.filter(function(s) {

            return s.id == soul.id_service;

          })

          //Poner en ocupado la sala

          roomService[0].available = "1";



          //Editar el status de la sala

          roomsAPI.updateData(roomService[0]);

        }



        var index = indexJsonArray($scope.soulsArray, id);

        $scope.soulsArray.splice(index, 1);



      }

    }



    $scope.nombreSala = function(id_service){

      var index = indexJsonArray($scope.roomsArray, id_service);



      return $scope.roomsArray[index].name;

    }



});



app.controller('salasCtrl', function($scope, $http, $sce, REST) {



  $scope.lastID = {"id" : 0};



  $scope.roomsArray = [];



  var roomsAPI = new REST(mainURL, "rooms");



  roomsAPI.getData($scope.roomsArray);



  $scope.disponible = function(id){

    return nombreAvailable(id);

  }



  $scope.labelDisponible = function(id){

    return colorAvailable(id);

  }



  $scope.registrarRoom = function(){



    $("#con-close-modal").modal('hide');



    $scope.formRoom.available = 1;



    roomsAPI.postData($scope.formRoom, $scope.lastID, $scope.roomsArray);

    

    $scope.formRoom = {};



  }



  $scope.filtrarRoom = function(id){

      $scope.roomEditar = [];

      $scope.roomEditar = $scope.roomsArray.filter(function(s) {

        return s.id == id;

      })

  }



  $scope.editarRoom = function(room){

      $("#edit-room-modal").modal('hide');

      roomsAPI.updateData(room);

  }



  $scope.eliminarRoom = function(id){

      var flag = confirm("¿Estás seguro que quieres borrar la sala #" + id + "?");

      

      if(flag && id != 1){

        //Arreglo JSON para acomodar el elemento y que el php pueda reconocerlo como objeto JSON

        roomsAPI.deleteData(id);



        var index = indexJsonArray($scope.roomsArray, id);

        $scope.roomsArray.splice(index, 1);

      }

  }



});



app.controller('religionesCtrl', function($scope, $http, $sce, REST) {



  $scope.lastID = {"id" : 0};



  $scope.religionsArray = [];



  var religionsAPI = new REST(mainURL, "religions");



  religionsAPI.getData($scope.religionsArray);





  $scope.registrarReligion = function(){



    $("#con-close-modal").modal('hide');



    religionsAPI.postData($scope.formReligion, $scope.lastID, $scope.religionsArray);

    

    $scope.formReligion = {};

  }



  $scope.eliminarReligion = function(id){

      var flag = confirm("¿Estás seguro que quieres borrar la religion #" + id + "?");

      

      if(flag){

        //Arreglo JSON para acomodar el elemento y que el php pueda reconocerlo como objeto JSON

        religionsAPI.deleteData(id);



        var index = indexJsonArray($scope.religionsArray, id);

        $scope.religionsArray.splice(index, 1);

      }

    }



});



app.controller('eventosCtrl', function($scope, $http, $sce, REST){



  $scope.eliminarEvent = function(id){

      var flag = confirm("¿Estás seguro que quieres borrar el evento #" + id + "?");

      

      if(flag){

        eventsAPI.deleteData(id);

        var index = indexJsonArray($scope.eventsArray, id);

        $scope.eventsArray.splice(index, 1);

      }

  }



  $scope.registrarEvent = function(){



    $("#con-close-modal").modal('hide');



    $scope.formE.start = $scope.formE.start + " " + $scope.hora;



    eventsAPI.postData($scope.formE, $scope.lastID, $scope.eventsArray);

    

    $scope.formE = {};

  }



  $scope.filtrarEvent = function(id){

      $scope.eventEditar = [];

      $scope.eventEditar = $scope.eventsArray.filter(function(s) {

        return s.id == id;

      })

  }



  $scope.editarEvent = function(event){

      $("#edit-room-modal").modal('hide');

      eventsAPI.updateData(event);

  }



  $scope.lastID = {"id" : 0};



  $scope.eventsArray = [];



  var eventsAPI = new REST(mainURL, "events");



  eventsAPI.getData($scope.eventsArray);



});



app.controller('detalleCtrl', function($scope, $http, $sce, REST) {



    $scope.labelEstado = function(idStatus){

      return colorEstado(idStatus);

    }



    $scope.estado = function(idStatus){

      return nombreEstado(idStatus);

    }



    $scope.nombreSala = function(id_service){

      var index = indexJsonArray($scope.roomsArray, id_service);



      return $scope.roomsArray[index].name;

    }



    $scope.nombreReligion = function(id){

      var index = indexJsonArray($scope.religionsArray, id);



      return $scope.religionsArray[index].name_religion;

    }



    $scope.filtrarSoul = function(id){

      $scope.filterRooms();

      $scope.soulEditar = [];

      $scope.soulEditar = $scope.soulsArray.filter(function(s) {

        return s.id == id;

      })



      salaVieja = $scope.soulEditar[0].id_service;



    }



    $scope.editarSoul = function(soul){

        // var archivo =  $scope.archivo;
        var archivo =  document.getElementById("file_soul"); 
        console.log('El archivo es ' );
        console.dir(archivo);


      if( errorFechas(soul.starting_date, soul.ending_date) ){

        alert("Estas ingresando una fecha de finalización errónea (Pasado)\nFecha de registro:" + soul.starting_date);

      }

      else{



        $("#editSoul-modal").modal('hide');

        $("#panel-modal").modal('hide');

        // soulsAPI.updateData(soul);



        //Si es un servicio local LIBERAR LA SALA QUE OCUPABA

        if(salaVieja != "1"){

          //Obtener la sala

          var roomService = [];



          roomService = $scope.roomsArray.filter(function(s) {

            return s.id == salaVieja;

          })

          //Poner en disponible la sala

          roomService[0].available = "1";



          //Editar el status de la sala

          roomsAPI.updateData(roomService[0]);

        }



        //Si es un servicio local OCUPAR LA NUEVA SALA

        if(soul.id_service != "1"){

          //Obtener la sala

          var roomService = [];



          roomService = $scope.roomsArray.filter(function(s) {

            return s.id == soul.id_service;

          })

          //Poner en disponible la sala

          roomService[0].available = "0";



          //Editar el status de la sala

          roomsAPI.updateData(roomService[0]);

        }



      }



    }



    $scope.registrarCelebration = function(){



      $scope.formC.start = $scope.formC.start + " " + $scope.horaS;

      $scope.formC.end = $scope.formC.end + " " + $scope.horaE;



      if(errorFechas($scope.formC.start, $scope.formC.end))

        alert("Estas ingresando una fecha de finalización errónea (Pasado)\nFecha de registro:" + $scope.formC.start);

      else{

        $("#celebracion-modal").modal('hide');



        $scope.formC.id_soul = $scope.celebration.id_soul;



        celebrationsAPI.postData($scope.formC, $scope.lastID, $scope.celebrationsArray);

        

        $scope.formC = {};

        $scope.horaE = "";

        $scope.horaS = "";

      }

      

    }



    $scope.filterRooms = function(){

      $scope.roomsAvailable = $scope.roomsArray.filter(function(s) {

        return s.available == "1";

      })

    }



    $scope.finalizarServicio = function(){

      //alert(JSON.stringify($scope.soulsArray[0]));

      var flag = confirm("¿Estás seguro de que se dará por finalizado el servicio?");

      

      if(flag){

        $scope.soulsArray[0].status = $scope.soulsArray[0].status + 1;

        soulsAPI.updateData($scope.soulsArray[0]);



        //Si es un servicio local

        if($scope.soulsArray[0].id_service != "1"){

          //Obtener la sala

          var roomService = [];



          roomService = $scope.roomsArray.filter(function(s) {

            return s.id == $scope.soulsArray[0].id_service;

          })

          //Poner en disponible la sala

          roomService[0].available = "1";



          //Editar el status de la sala

          roomsAPI.updateData(roomService[0]);

        }



      }

    }



    $scope.restaurarServicio = function(){

      //alert(JSON.stringify($scope.soulsArray[0]));

      var flag = confirm("¿Se volverá a poner el servicio en proceso?");

      

      if(flag){

        $scope.soulsArray[0].status = $scope.soulsArray[0].status - 1;

        soulsAPI.updateData($scope.soulsArray[0]);



         //Si es un servicio local

        if($scope.soulsArray[0].id_service != "1"){

          //Obtener la sala

          var roomService = [];



          roomService = $scope.roomsArray.filter(function(s) {

            return s.id == $scope.soulsArray[0].id_service;

          })

          //Poner en disponible la sala

          roomService[0].available = "0";



          //Editar el status de la sala

          roomsAPI.updateData(roomService[0]);

        }



      }

    }



    $scope.lastID = {"id" : 0};

    $scope.celebration = {"id_soul" : varURL.id};



    $scope.soulsArray = [];

    $scope.roomsArray = [];

    $scope.religionsArray = [];

    $scope.celebrationsArray = [];

    $scope.roomsAvailable = [];

    var salaVieja = "";



    var soulsAPI = new REST(mainURL, "souls");

    var roomsAPI = new REST(mainURL, "rooms");

    var religionsAPI = new REST(mainURL, "religions");

    var celebrationsAPI = new REST(mainURL, "celebrations");



    soulsAPI.getData($scope.soulsArray, $scope.celebration.id_soul);

    roomsAPI.getData($scope.roomsArray);

    religionsAPI.getData($scope.religionsArray);

    celebrationsAPI.getData($scope.celebrationsArray);



});



app.controller('esquelaCtrl', function($scope, $http, $sce, REST){



  $scope.verAno = function(cadena){

    return ano(cadena);

  }



  $scope.formatoFecha = function(cadena){

    return dia(cadena) + " de " + mes(cadena) + " de " + ano(cadena);

  }



  $scope.nombreSala = function(id_service){

      var index = indexJsonArray($scope.roomsArray, id_service);



      return $scope.roomsArray[index].name;

  }



  $scope.imagen = function(cadena){

    if(cadena == "assets/images/flor.jpg")

      return "../admin/assets/images/flor.jpg";

    else

      return cadena;

  }



  $scope.hola= varURL.id;

  $scope.celebration = {"id_soul" : varURL.id};



  $scope.soulsArray = [];

  $scope.roomsArray = [];

  $scope.celebrationsArray = [];



  var soulsAPI = new REST(mainURL, "souls");

  var roomsAPI = new REST(mainURL, "rooms");

  var celebrationsAPI = new REST(mainURL, "celebrations");



  soulsAPI.getData($scope.soulsArray, varURL.id);

  roomsAPI.getData($scope.roomsArray);

  celebrationsAPI.getData($scope.celebrationsArray);



});



app.controller('publicidadCtrl', function($scope, $http, $sce, REST) {



  $scope.lastID = {"id" : 0};



  $scope.advertisingArray = [];



  var advertisingAPI = new REST(mainURL, "advertising");



  advertisingAPI.getData($scope.advertisingArray);





  $scope.registrarAdvertising = function(){



    $("#con-close-modal").modal('hide');



    advertisingAPI.postData($scope.formAdvertising, $scope.lastID, $scope.advertisingArray);

    

    $scope.formAdvertising = {};

  }



  $scope.eliminarAdvertising = function(id){

      var flag = confirm("¿Estás seguro que quieres borrar la publicidad #" + id + "?");

      

      if(flag){

        //Arreglo JSON para acomodar el elemento y que el php pueda reconocerlo como objeto JSON

        advertisingAPI.deleteData(id);



        var index = indexJsonArray($scope.advertisingArray, id);

        $scope.advertisingArray.splice(index, 1);

      }

  }



  $scope.filtrarAdvertising = function(id){

      $scope.advertisingEditar = [];

      $scope.advertisingEditar = $scope.advertisingArray.filter(function(s) {

        return s.id == id;

      })

  }



  $scope.editarAdvertising = function(a){

      $("#edit-room-modal").modal('hide');

      advertisingAPI.updateData(a);

  }



  $scope.available = function(a){

    if(a == "0")

      return "Oculto";

    else

      return "Visible";

  }



});



app.controller('pantallasCtrl', function($scope, $http, $sce, REST){



  $scope.registrarScreen = function(){

    $("#con-close-modal").modal('hide');



    screensAPI.postData($scope.formS, $scope.lastID, $scope.screensArray);

    

    $scope.formAdvertising = {};

  }



  $scope.nombreSala = function(id_service){

      var index = indexJsonArray($scope.roomsArray, id_service);



      return $scope.roomsArray[index].name;

  }



  $scope.eliminarScreen = function(id){

    var flag = confirm("¿Estás seguro que quieres borrar la pantalla ?");

      

      if(flag){

        //Arreglo JSON para acomodar el elemento y que el php pueda reconocerlo como objeto JSON

        screensAPI.deleteData(id);



        var index = indexJsonArray($scope.screensArray, id);

        $scope.screensArray.splice(index, 1);

      }

  }



  $scope.filtrarScreen = function(id){

      $scope.screenEditar = [];

      $scope.screenEditar = $scope.screensArray.filter(function(s) {

        return s.id == id;

      })

  }



  $scope.editarScreen = function(a){

      $("#edit-room-modal").modal('hide');

      screensAPI.updateData(a);

  }



  $scope.lastID = {"id" : 0};

  $scope.screensArray = [];

  $scope.roomsArray = [];



  var screensAPI = new REST(mainURL, "screens");

  var roomsAPI = new REST(mainURL, "rooms");



  screensAPI.getData($scope.screensArray);

  roomsAPI.getData($scope.roomsArray);

});



angular.bootstrap(document.getElementById("App"),['myApp']);