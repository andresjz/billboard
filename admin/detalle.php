<!DOCTYPE html>

<html>

    <head>

        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">

        <meta name="author" content="Coderthemes">



        <link rel="shortcut icon" href="assets/images/favicon.ico">



        <title>Cartelera</title>



        <!--Morris Chart CSS -->

		<link rel="stylesheet" href="assets/plugins/morris/morris.css">



        <!-- form Uploads -->

        <link href="assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />



        

         <!-- Plugins css-->

        <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />

        <link href="assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />

        <link href="assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">

        <link href="assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">

        <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

        <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">

		<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">

		<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">

		<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        

        <!-- DataTables -->

        <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

        <link href="assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        

        

        <!-- App css -->

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />

        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />

        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />

        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />

        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />



        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->

        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <!--[if lt IE 9]>

        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

        <![endif]-->



        <script src="assets/js/modernizr.min.js"></script>





    </head>





    <body class="fixed-left" id="App" ng-controller="detalleCtrl">

        

        <div id="editSoul-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

                <div class="modal-dialog">

                    <div class="modal-content">

                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                            <h4 class="modal-title">Editar datos de servicio</h4>

                        </div>

                        <div class="modal-body" ng-repeat="s in soulEditar">

                            <form name="myform" novalidate>

                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="form-group">

                                            <label for="field-1" class="control-label">Nombre del finado</label>

                                            <input type="text" class="form-control" id="field-1" ng-model="s.full_name" required>

                                        </div>

                                    </div>



                                    <div class = "col-md-6">

                                        <div class="form-group">

                                            <label class="control-label">Fecha de muerte</label>

                                            <input type="date" class="form-control" ng-model="s.death_date" required>

                                        </div>

                                    </div>



                                    <div class="col-md-6">

                                        <div class="form-group">

                                            <label for="field-1" class="control-label">Clave interna</label>

                                            <input type="text" class="form-control" id="field-1" ng-model="s.intern_code" required>

                                        </div>

                                    </div>



                                    <div class="col-md-6">

                                        <div class="form-group">

                                            <label for="field-2" class="control-label">Tipo de servicio</label>

                                            <select class="form-control" ng-model="s.type_service" required>

                                               <option value="Cremacion">Cremación</option>

                                               <option value="Inhumacion">Inhumación</option>

                                           </select>

                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group">

                                            <label for="field-3" class="control-label">Lugar de velación</label>

                                            <select class="form-control" required ng-model="s.id_service" ng-options="room.id as ( room.name ) for room in roomsAvailable"></select>

                                        </div>

                                    </div>

                                </div>

                      

                                <div class="row">

                                    <div class="col-md-12" ng-show="s.id_service == 1">

                                        <div class="form-group">

                                            <label for="field-4" class="control-label">Dirección</label>

                                            <input type="text" class="form-control" id="field-4" ng-model="s.address">

                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="form-group">

                                            <label for="field-4" class="control-label">Religión</label>

                                            <select class="form-control" required ng-model="s.icon_id" ng-options="religion.id as ( religion.name_religion ) for religion in religionsArray"></select>

                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="form-group">

                                          <label class="control-label col-sm-4">Fecha y hora de finalización</label>

                                              <div class="col-sm-8">

                                                 <div class="input-group">

                                                    <input type="datetime" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose" ng-model="s.ending_date" required>

                                                    <span  class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>

                                                 </div><!-- input-group -->

                                              </div>

                                       </div>

                                    </div>

                                    

                                </div>



                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="form-group">

                                            <label for="field-4" class="control-label">Correo electrónico</label>

                                            <input type="email" class="form-control" name="mail" ng-model="s.email" required>

                                        </div>

                                    </div>

                                </div>





                                <div class="modal-footer">

                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>

                                    <button type="submit" class="btn btn-info waves-effect waves-light" ng-click="editarSoul(s)" ng-show="myform.$valid">Guardar cambios</button>

                                </div>

                            </form>

                        </div>

                </div>

            </div>

        </div>

        

        <div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

            <div class="modal-dialog">

                <div class="modal-content p-0 b-0">

                    <div class="panel panel-color panel-primary" ng-repeat="s in soulEditar">

                        <div class="panel-heading">

                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>

                            <h3 class="panel-title">Actualización de fotografía</h3>

                        </div>

                        <div class="panel-body">

                            <!--

                            <div class="card-box">

                                <div class="dropify-wrapper" style="height: 314px;"><div class="dropify-message"><span class="file-icon"></span> <p>Arrastra un archivo o da clic para agregar una nueva imagen</p><p class="dropify-error">Parece que algo salió mal, intentalo de nuevo</p></div><div class="dropify-loader"></div><div class="dropify-errors-container"><ul></ul></div><input type="file" class="dropify" data-height="300"><button type="button" class="dropify-clear">Eliminar</button><div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p><p class="dropify-infos-message">Arrastra un archivo o da clic para remplazar</p></div></div></div></div>

                            </div>

                            -->



                            <div class="row">

                                    <div class="col-md-12">

                                        <div class="form-group">

                                            <label for="field-4" class="control-label">URL Fotografía (temporal)</label>

                                            <input type="text" class="form-control" id="field-4" ng-model="s.path_img">

                                        </div>

                                    </div>

                            </div>

                            <div class="row">

                                <div class="panel-body">

                                    <div class="card-box">

                                        <div class="dropify-wrapper" style="height: 314px;"><div class="dropify-message"><span class="file-icon"></span> <p>Arrastra un archivo o da clic para agregar una nueva imagen</p><p class="dropify-error">Parece que algo salió mal, intentalo de nuevo</p></div><div class="dropify-loader"></div><div class="dropify-errors-container"><ul></ul></div><input type="file" ng-model="s.file_soul" id="file_soul" class="dropify" data-height="300"><button type="button" class="dropify-clear">Eliminar</button><div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p><p class="dropify-infos-message">Arrastra un archivo o da clic para remplazar</p></div></div></div></div>

                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">

                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>

                                <button type="button" class="btn btn-info waves-effect waves-light" ng-click="editarSoul(s)">Guardar cambios</button>

                            </div>

                        </div>



                        

                    </div>

                </div><!-- /.modal-content -->



            </div><!-- /.modal-dialog -->

        </div><!-- /.modal -->

        

        <div id="celebracion-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

                                        <div class="modal-dialog">

                                            <div class="modal-content">

                                                <div class="modal-header">

                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                    <h4 class="modal-title">Nueva celebración</h4>

                                                </div>

                                                <div class="modal-body">

                                                    

                                                    <div class="row">

                                                        <div class="col-md-12">

                                                            <div class="form-group">

                                                                <label for="field-4" class="control-label">Título de la celebración</label>

                                                                <input type="text" class="form-control" id="field-4" ng-model="formC.name">

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-12">

                                                            <div class="form-group">

                                                                <label for="field-4" class="control-label">Descripción</label>

                                                                <input type="text" class="form-control" id="field-4">

                                                            </div>

                                                        </div>

                                                    </div>

                                                        

                                                    <div class="row">

                                                        <div class="col-md-12">

                                                            <div class="form-group">

                                                                <label for="field-4" class="control-label">Lugar</label>

                                                                <input type="text" class="form-control" id="field-4" ng-model="formC.place">

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-6">

                                                            <div class="form-group">

			                                			      <label class="control-label ">Fecha de inicio</label>

			                                			        

																        <input type="date" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose" ng-model="formC.start">

																      

			                                		       </div>

                                                        </div>

                                                        <div class="col-md-6">

			                                			      <label class="control-label">Hora</label>

                                                                    <input id="timepicker3" type="time" class="form-control" ng-model="horaS">

                                                                

                                                        </div>

                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-6">

                                                            <div class="form-group">

			                                			      <label class="control-label">Fecha de finalización</label>

			                                			      

																        <input type="date" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose" ng-model="formC.end">

																       

			                                		       </div>

                                                        </div>

                                                        <div class="col-md-6">

			                                			      <label class="control-label">Hora</label>

                                                            

                                                                    <input id="timepicker3" type="time" class="form-control" ng-model="horaE">

                                                               

                                                        </div>

                                                    </div>

                                                

                                                        

                                                        

                                                    </div>

                                                <div class="modal-footer">

                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>

                                                    <button type="button" class="btn btn-info waves-effect waves-light" ng-click="registrarCelebration()" >Guardar cambios</button>

                                                </div>

                                            </div>

                                        </div>

            

        </div>

        

        <!-- Begin page -->

        <div id="wrapper">



            <!-- Top Bar Start -->

            <div class="topbar">



                <!-- LOGO -->

                <div class="topbar-left">

                    <a href="index.html" class="logo"><span>Cartelera Digital</span><i class="zmdi zmdi-layers"></i></a>

                </div>



                <!-- Button mobile view to collapse sidebar menu -->

                <div class="navbar navbar-default" role="navigation">

                    <div class="container">



                        <!-- Page title -->

                        <ul class="nav navbar-nav navbar-left">

                            <li>

                                <button class="button-menu-mobile open-left">

                                    <i class="zmdi zmdi-menu"></i>

                                </button>

                            </li>

                            <li>

                                <h4 class="page-title">Hola, usuario</h4>

                            </li>

                        </ul>



                        <!-- Right(Notification and Searchbox -->

                        <ul class="nav navbar-nav navbar-right">

                            <li>

                                <!-- Notification -->

                                <h5>Ayuda</h5>

                                <!-- End Notification bar -->

                            </li>

                            

                        </ul>



                    </div><!-- end container -->

                </div><!-- end navbar -->

            </div>

            <!-- Top Bar End -->

           

            

            <!-- ========== Left Sidebar Start ========== -->

            <div class="left side-menu">

                <div class="sidebar-inner slimscrollleft">



                    <!-- User -->

                    <div class="user-box">

                        <div class="user-img">

                            <img src="assets/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">

                            <div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>

                        </div>

                        <h5><a href="#">Funeraria Vélez</a> </h5>

                        

                    </div>

                    <!-- End User -->



                    <!--- MENU -->

                    <div id="sidebar-menu">

                        <ul>

                        	<li class="text-muted menu-title">Menú</li>



                            <li>

                                <a href="index.html" class="waves-effect active"><i class="zmdi zmdi-view-dashboard"></i> <span> Servicios </span> </a>

                            </li>

                            <li>

                                <a href="salas.html" class="waves-effect"><i class="zmdi zmdi-collection-text"></i><span> Salas </span> </a>

                            </li>



                            <li class="has_sub">

                                <a href="religiones.html" class="waves-effect"><i class="zmdi zmdi-view-list"></i> <span> Religiones </span></a>

                            </li>

                            <li>

                                <a href="pantallas.html" class="waves-effect"><i class="zmdi zmdi-calendar"></i><span> Pantallas </span></a>

                            </li>

                            <li>

                                <a href="eventos.html" class="waves-effect"><i class="zmdi zmdi-calendar"></i><span> Eventos </span></a>

                            </li>



                            <li>

                                <a href="publicidad.html" class="waves-effect"><i class="zmdi zmdi-email"></i><span class="label label-purple pull-right">A</span><span> Publicidad </span></a>

                            </li>



                            

                            

                        </ul>

                        <div class="clearfix"></div>

                    </div>

                    

                    <!-- Sidebar -->

                    <div class="clearfix"></div>



                </div>



            </div>

            <!-- Left Sidebar End -->







            <!-- ============================================================== -->

            <!-- Start right Content here -->

            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Start content -->

                <div class="content">

                    <div class="container">

                        



                        <div class="row" ng-repeat="soul in soulsArray">

                            <div class="col-sm-8">

                                <div class="bg-picture card-box">

                                    <div class="profile-info-name">

                                        <img src="{{soul.path_img}}"

                                             class="img-thumbnail" alt="profile-image">



                                        <div class="profile-info-detail">

                                            <h3 class="m-t-0 m-b-0">{{soul.full_name}} {{soul.path_image}}</h3>

                                            <div class="label label-{{labelEstado(soul.status)}}">{{estado(soul.status)}}</div>

                                            <h4 class="text-muted m-b-20"><i>{{ nombreSala(soul.id_service) }}</i></h4>

                                            

                                            <table class="table table table-hover m-0">

                                    

                                        <tbody>

                                            <tr ng-show="soul.id_service == 1">

                                                <td>Dirección de velación</td>

                                                <td>{{soul.address}}</td>

                                                



                                            </tr>

                                            <tr>

                                                <td>Fecha de registro</td>

                                                <td>{{soul.starting_date}}</td>

                                                

                                            </tr>

                                            <tr>

                                                <td>Fecha de finalización</td>

                                                <td>{{soul.ending_date}}</td>

                                            </tr>

                                            <tr>

                                                <td>Religión</td>

                                                <td>{{nombreReligion(soul.icon_id)}}</td>

                                            </tr>

                                            <tr>

                                                <td>Correo electrónico</td>

                                                <td>{{soul.email}}</td>

                                            </tr>

                                            <tr>

                                                <td>Estado del servicio</td>

                                                <td><span class="label label-{{labelEstado(soul.status)}}">{{estado(soul.status)}}</span></td>

                                            </tr>

                                            <tr>

                                                <td>Clave interna</td>

                                                <td>{{soul.intern_code}}</td>

                                            </tr>

                                            

                                            

                                            

                                        </tbody>

                                    </table>

                                            

                                        </div>



                                        <div class="clearfix"></div>

                                    </div>

                                </div>

                                <!--/ meta -->





                                





                            </div>



                            <div class="col-sm-4">

                                <div class="card-box">

                                    



                        			<h4 class="header-title m-t-0 m-b-30"><b>Opciones</b></h4>



                                    <ul class="list-group m-b-0 user-list team-list">



                                        <li class="list-group-item">

                                            <a class="user-list-item">

                                            <button type="button" class="btn btn-block btn-sm btn-default waves-effect waves-light" data-toggle="modal" data-target="#editSoul-modal" ng-click="filtrarSoul(soul.id)">Editar datos</button>

                                            </a>

                                        </li>



                                        <li class="list-group-item">

                                            <a class="user-list-item">

                                            <button type="button" class="btn btn-block btn-sm btn-default waves-effect waves-light" data-toggle="modal" data-target="#panel-modal" ng-click="filtrarSoul(soul.id)">Agregar fotografía</button>

                                            </a>

                                        </li>

                                        

                                        <li class="list-group-item">

                                            <a href="#" class="user-list-item">

                                            <button type="button" class="btn btn-block btn-sm btn-default waves-effect waves-light" data-toggle="modal" data-target="#celebracion-modal">Agregar celebración</button>

                                            </a>

                                        </li>



                                        <li class="list-group-item">

                                            <a href="#" class="user-list-item">

                                            <button type="button" class="btn btn-block btn-sm btn-danger waves-effect waves-light" ng-click="finalizarServicio()" ng-show="soul.status == 0">Finalizar servicio</button>

                                            </a>

                                        </li>



                                        <li class="list-group-item">

                                            <a href="#" class="user-list-item">

                                            <button type="button" class="btn btn-block btn-sm btn-warning waves-effect waves-light" ng-click="restaurarServicio()" ng-show="soul.status == 1">Reestablecer servicio</button>

                                            </a>

                                        </li>



                                        <li class="list-group-item">

                                            <a href="../esquela/index.php?id={{soul.id}}" target="_BLANK" class="user-list-item">

                                                <button type="button" class="btn btn-block btn-sm btn-success waves-effect waves-light" >Ver esquela</button>

                                            </a>

                                        </li>



                                    </ul>



                                </div>



                                <div class="card-box">

                                    



                        			<h4 class="header-title m-t-0 m-b-30"><i class="zmdi zmdi-notifications-none m-r-5"></i> Celebraciones </h4>



                                    <ul class="list-group m-b-0 user-list team-list">

                                        <li class="list-group-item" ng-repeat="c in celebrationsArray | filter: celebration.id_soul">

                                            <a href="#" class="user-list-item">

                                                <div class="avatar text-center">

                                                    <i class="zmdi zmdi-circle text-primary"></i>

                                                </div>

                                                <div class="user-desc">

                                                    <span class="name">{{c.name}}</span>

                                                    <span class="desc">{{c.start}} - {{c.end}}</span>

                                                </div>

                                            </a>

                                        </li>

                                    </ul>



                                </div>



                            </div>

                        </div>



                    </div> <!-- container -->



                </div> <!-- content -->



                <footer class="footer text-right">

                    2016 © Distrito Digital - Innovación Tecnológica.

                </footer>



            </div>





            <!-- ============================================================== -->

            <!-- End Right content here -->

            <!-- ============================================================== -->





            <!-- /Right-bar -->



        </div>



                <footer class="footer text-right">

                    2016 © Distrito Digital - Innovación Tecnológica.

                </footer>



            </div>





            <!-- ============================================================== -->

            <!-- End Right content here -->

            <!-- ============================================================== -->





            <!-- /Right-bar -->



        </div>

        <!-- END wrapper -->







        <script>

            var resizefunc = [];

        </script>



        <!-- file uploads js -->

        <script src="assets/plugins/fileuploads/js/dropify.min.js"></script>



        <!-- App js -->

        <script src="assets/js/jquery.core.js"></script>

        <script src="assets/js/jquery.app.js"></script>



        <script type="text/javascript">

            $('.dropify').dropify({

                messages: {

                    'default': 'Drag and drop a file here or click',

                    'replace': 'Drag and drop or click to replace',

                    'remove': 'Remove',

                    'error': 'Ooops, something wrong appended.'

                },

                error: {

                    'fileSize': 'The file size is too big (1M max).'

                }

            });

        </script>

        

        <!-- jQuery  -->

        <script src="assets/js/jquery.min.js"></script>

        <script src="assets/js/bootstrap.min.js"></script>

        <script src="assets/js/detect.js"></script>

        <script src="assets/js/fastclick.js"></script>

        <script src="assets/js/jquery.slimscroll.js"></script>

        <script src="assets/js/jquery.blockUI.js"></script>

        <script src="assets/js/waves.js"></script>

        <script src="assets/js/jquery.nicescroll.js"></script>

        <script src="assets/js/jquery.slimscroll.js"></script>

        <script src="assets/js/jquery.scrollTo.min.js"></script>



        <!-- KNOB JS -->

        <!--[if IE]>

        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>

        <![endif]-->

        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>



        <!--Morris Chart-->

		<script src="assets/plugins/morris/morris.min.js"></script>

		<script src="assets/plugins/raphael/raphael-min.js"></script>



        <!-- Dashboard init -->

        <script src="assets/pages/jquery.dashboard.js"></script>



        <!-- App js -->

        <script src="assets/js/jquery.core.js"></script>

        <script src="assets/js/jquery.app.js"></script>



        <script>

            var varURL = {

                id : <?php echo json_encode($_GET["id"]); ?>

            };

        </script>



        <script type="text/javascript" src="app/angular.min.js"></script>

        <script type="text/javascript" src="app/app.js"></script>

        <script type="text/javascript" src="app/controllers.js"></script>



    </body>

</html>