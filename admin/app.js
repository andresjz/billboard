angular.moduel('appCartelera', ['ui.router'])
	.config(function($stateProvider, $urlRouterProvider){
		$stateProvider.
		state('main', {
			url: '/main', 
			templateUrl: 'vistas/main.html'
		})
		.state('servicios', {
			url: '/servicios',
			templateUrl: 'vistas/servicios.html'
		})
		$urlRouterProvider.otherwise('main'); 
	})