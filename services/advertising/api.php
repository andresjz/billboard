<?php
 	require_once("Rest.inc.php");
	
	class API extends REST {
	
		public $data = "";
		
		const DB_SERVER = "127.0.0.1";
		const DB_USER = "lionsys";
		const DB_PASSWORD = "Syslion894";
		const DB = "multiscreens";

		private $db = NULL;
		private $mysqli = NULL;
		public function __construct(){
			parent::__construct();				// Init parent contructor
			$this->dbConnect();					// Initiate Database connection
		}
		
		/*
		 *  Connect to Database
		*/
		private function dbConnect(){
			$this->mysqli = new mysqli(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD, self::DB);
		}
		
		/*
		 * Dynmically call the method based on the query string
		 */
		public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['x'])));
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404); // If the method not exist with in this class "Page not found".
		}
				
		/*private function login(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$email = $this->_request['email'];		
			$password = $this->_request['pwd'];
			if(!empty($email) and !empty($password)){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					$query="SELECT uid, name, email FROM users WHERE email = '$email' AND password = '".md5($password)."' LIMIT 1";
					$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);

					if($r->num_rows > 0) {
						$result = $r->fetch_assoc();	
						// If success everythig is good send header as "OK" and user details
						$this->response($this->json($result), 200);
					}
					$this->response('', 204);	// If no records "No Content" status
				}
			}
			
			$error = array('status' => "Failed", "msg" => "Invalid Email address or Password");
			$this->response($this->json($error), 400);
		}*/
		
		private function get(){	
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			$query="SELECT * FROM advertising order by id desc";
			$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);

			if($r->num_rows > 0){
				$result = array();
				while($row = $r->fetch_assoc()){
					$result[] = $row;
				}
				$this->response($this->json($result), 200); // send user details
			}
			$this->response('',204);	// If no records "No Content" status
		}
		private function getAdvertising(){	
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			$id = (int)$this->_request['id'];
			if($id > 0){
				$result = array();
				$query="SELECT * FROM advertising where id =$id";
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				if($r->num_rows > 0) {
					$result = array();
					while($row = $r->fetch_assoc()){
						$result[] = $row;
					}
					$this->response($this->json($result), 200); // send user details
				}
			}
			$this->response('',204);	// If no records "No Content" status
		}
		
		private function insert(){
			// $queryId = "SELECT id FROM advertising ORDER BY id DESC LIMIT 1";
			// $result = 	$mysqli->query($queryId);
			// $row = mysqli_fetch_array($result);
			// $id	= $row['id'];
			// $id	= $id + 1;

			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}

			$soul = json_decode(file_get_contents("php://input"),true);
			$column_names = array('name', 'description', 'url_image', 'website');
			//echo $soul;
			$keys = array_keys($soul);
			$columns = '';
			$values = '';
			foreach($column_names as $desired_key){ // Check the soul received. If blank insert blank into the array.
			   if(!in_array($desired_key, $keys)) {
			   		$$desired_key = '';
				}else{
					$$desired_key = $soul[$desired_key];
				}
				$columns = $columns.$desired_key.',';
				$values = $values."'".$$desired_key."',";
			}
			$query = "INSERT INTO advertising (".trim($columns,',').") VALUES(".trim($values,',').")";
			// echo $query;
			if(!empty($soul)){
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				$success = array('status' => "Success", "msg" => "soul Created Successfully.", "id" => $this->mysqli->insert_id, "data" => $soul);
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	//"No Content" status
		}
/*
		private function update(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$soul = json_decode(file_get_contents("php://input"),true);
			$column_names = array('name', 'description', 'url_image', 'website', 'available');
			$keys = array_keys($soul['id']);
			$columns = '';
			$values = '';
			foreach($column_names as $desired_key){ // Check the soul received. If key does not exist, insert blank into the array.
			   if(!in_array($desired_key, $keys)) {
			   		$$desired_key = '';
				}else{
					$$desired_key = $soul['soul'][$desired_key];
				}
				$columns = $columns.$desired_key."='".$$desired_key."',";
			}
			$query = "UPDATE advertising SET ".trim($columns,',')." WHERE id=$id";
			if(!empty($soul)){
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				$success = array('status' => "Success", "msg" => "soul ".$id." Updated Successfully.", "data" => $soul);
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	// "No Content" status
		}*/

		private function update(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$soul = json_decode(file_get_contents("php://input"),true);
			$column_names = array('name', 'description', 'url_image', 'website', 'available');
			$keys = array_keys($soul);
			$id = (int)$soul['id'];
			$columns = '';
			$values = '';
			foreach($column_names as $desired_key){ // Check the soul received. If key does not exist, insert blank into the array.
			   if(!in_array($desired_key, $keys)) {
			   		$$desired_key = '';
				}else{
					$$desired_key = $soul[$desired_key];
				}
				$columns = $columns.$desired_key."='".$$desired_key."',";
			}
			$query = "UPDATE advertising SET ".trim($columns,',')." WHERE id=$id";
			if(!empty($soul)){
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				$success = array('status' => "Success", "msg" => "soul ".$id." Updated Successfully.", "data" => $soul, "query" => $query);
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	// "No Content" status
		}
		
		private function delete(){
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			$id = (int)$this->_request['id'];
			if($id > 0){				
				$query="DELETE FROM advertising WHERE id = $id";
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				$success = array('status' => "Success", "msg" => "Successfully deleted one record.");
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	// If no records "No Content" status
		}
		
		/*
		 *	Encode array into JSON
		*/
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}
	}
	
	// Initiiate Library
	
	$api = new API;
	$api->processApi();
?>